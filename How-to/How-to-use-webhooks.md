We'll explain how you can use webhooks with `tbot`, including testing on your
local machine and configuring it on your server.

## Testing on your local machine

You can use [`ngrok`] to test webhooks on your machine. Follow their steps
to install it on your computer. After you installed it, run this command:

```bash
./ngrok http $PORT
```

Remember the port you're listening on and the HTTPS server's URL [`ngrok`]
provided you with. Then, you configure `tbot`:

```rust
let bot = tbot::bot!("BOT_TOKEN").event_loop();
// ..
bot.webhook(URL, PORT).http().start().await.unwrap();
```

[`ngrok`]: https://ngrok.com

## Configuring your server

First of all, you need to know your server's address so Telegram will be able to
send you updates. Second, you need to get an HTTPS certificate. Using a trusted
certificate will be easier, but such a certificate costs money. You can, though,
[generate][self signed] a self-signed certificate, but it requires a few more
steps to configure.

Then, you can choose one of two ways.

[self signed]: https://core.telegram.org/bots/self-signed

### Proxying with `nginx`

If you choose this way, you will need to have `nginx` already installed on your
server and properly configured, including HTTPS, and, per [docs][setwebhook],
running on one of these ports: `443`, `80`, `83` and `8443`. Using `nginx` will
allow you to use the same IP/domain and port for as many bots as you want
(remember, though, that `tbot` can only run one bot on one port). In your
`nginx` config, you'll need to add a `location` block:

```txt
server {
    // ..
    location <PATH> {
        proxy_pass localhost:<PORT>/;
    }
}
```

Replace `<PORT>` with a port on which `tbot` will be running, and `<PATH>`
with a path Telegram will be making requests to. For example, if your bot is
running on `localhost:4242`, and you want Telegram to send requests to
`https://<your_ip_or_domain>/my_bot` (we don't recommend this path, as you
should use a hard-to-guess one instead), the `location` block will look like
this:

```txt
location /my_bot {
    proxy_pass localhost:4242/;
}
```

> If you, for some reason, configure `tbot` to only accept requests to some
> other path than `/`, don't forget to add it to `proxy_pass`'s value.

Now, you need to configure `tbot`. The port which you want `tbot` to bind to
should be closed from public access. Also, as HTTPS is handled by `nginx`,
`tbot` may run an HTTP server. So, this is how you configure `tbot`:

```rust
let bot = tbot::bot!("BOT_TOKEN").event_loop();
// ..
bot.webhook(URL, PORT).http().start().await.unwrap();
```

If you use a self-signed certificate, you need to include the public key for
Telegram to trust your certificate:

```rust
bot.webhook(URL, PORT)
    .certificate(include_str!("path/to/public/key/cert.pem"))
    .http()
    .start()
    .await
    .unwrap();
```

### Running `tbot` on a public port directly

You can also start an HTTPS server with `tbot`, so that you don't need any proxy
between Telegram and `tbot`.

First, you need to initialize an [`Identity`] (learn more about it in its docs):

```rust
use tbot::event_loop::webhook::https::Identity;
let bot = tbot::from_env!("BOT_TOKEN").event_loop();
// ..
let identity = Identity::from_pkcs12(
    include_bytes!("path/to/identity.p12"),
    env!("IDENTITY_PASSWORD"),
).unwrap();
```

> If you don't have an identity (a PKCS #12 archive) for your certificate yet,
> run this command to create one based on your private key and your certificate:
>
> ```bash
> openssl pkcs12 -export -out identity.p12 -inkey private.key -in cert.pem
> ```

Then, you pass `identity` to `tbot`:

```rust
// ..
let identity: Identity = ..;
bot.webhook(URL, PORT).https(identity).start().await.unwrap();
```

If you have a self-signed certificate, you need to include the public key for
Telegram to trust your certificate:

```rust
bot.webhook(URL, PORT)
    .certificate(include_str!("path/to/public/key/cert.pem"))
    .https(identity)
    .start()
    .await
    .unwrap();
```

Remember that per [docs][setwebhook], you may start the bot only on these ports:
`443`, `80`, `83` and `8443`.

[`identity`]: https://snejugal.gitlab.io/tbot/tbot/event_loop/webhook/https/struct.Identity.html
[setwebhook]: https://core.telegram.org/bots/api#setwebhook

## Notes

- `tbot` listens to connections on `127.0.0.1` by default, you can change it
  with [`Webhook::ip`].
- `tbot` ignores non-POST requests, or if the request is not sent to the allowed
  path (`/` by default).

[`webhook::ip`]: https://docs.rs/tbot/0.3/tbot/event_loop/struct.Webhook.html#method.ip
